import numpy as np
import cv2
import random
	
video = cv2.VideoCapture(0)	
#video2 = cv2.VideoCapture("highway.mp4")			#opens camera

kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
bgSub = cv2.BackgroundSubtractorMOG()		#initializes background subtractor
"""
kernel2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3))
bgSub2 = cv2.BackgroundSubtractorMOG()	
"""
while(True):
	ret, frame = video.read()
	#ret2, frame2 = video2.read()				#reads videos
	myframe = bgSub.apply(frame)			#gets frames from video
	myframe = cv2.morphologyEx(myframe, cv2.MORPH_OPEN, kernel)		#subtracts background from first frame
	#myframe2 = bgSub2.apply(frame2)			#gets frames from video
	#myframe2 = cv2.morphologyEx(myframe2, cv2.MORPH_OPEN, kernel2)		#subtracts background from first frame
	cv2.imshow('frame', myframe)			#show frame with subtracted background
	#cv2.imshow('frame2',myframe2)
	height, width, x = frame.shape
	whitePixels = float(cv2.countNonZero(myframe))		#gets white pixels from the frame
	dimension = height*width
	density = (whitePixels/dimension)*100		#divides number of white pixels to total pixels to get density (in percentage)
	"""
	height2, width2, x2 = frame2.shape
	whitePixels2 = float(cv2.countNonZero(myframe2))		#gets white pixels from the frame
	dimension2 = height2*width2
	density2 = (whitePixels2/dimension2)*100
	"""
	density2=50+density*random.random()/2
	density3=89+density*random.random()/2
	density4=67+density*random.random()/2
	density5=57+density*random.random()/2
	density6=56+density*random.random()/2
	density7=55+density*random.random()/2
	density8=85+density*random.random()/2
	density9=14+density*random.random()/2
	density10=73+density*random.random()/2
	density11=65+density*random.random()/2
	density12=55+density*random.random()/2
	density13=60+density*random.random()/2
	myfile = open("message.txt", 'w')
	myfile.write(str(density)+'\n'+str(density2)+'\n'+str(density3)+'\n'+str(density4)+'\n'+str(density5)+'\n'+str(density6)+'\n'+str(density7)+'\n'+str(density8)+'\n'+str(density9)+'\n'+str(density10)+'\n'+str(density11)+'\n'+str(density12)+'\n'+str(density13))
	myfile.close()
	k = cv2.waitKey(30) & 0xff
	if k == 27:
		break
video.release()							#exits camera
#video2.release()
cv2.destroyAllWindows()					